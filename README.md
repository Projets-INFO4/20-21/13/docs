# Fiche de Suivi

## Séance 1 - Lundi 18 Janvier

Choix du projet

## Séance 2 - Lundi 25 Janvier

Présentation du projet

## Séance 3 - Lundi 1 Février

Mise en place du projet / Création des deux dépôts git  
Première ébauche du plan de travail (Analyse des besoins et scénarios)  
Appropriation des exports ADE et des identifiants de filières  

## Séance 4 - Lundi 8 Février

Rajouts sur le plan de travail et scénarios  
Création des répertoires src et data contenant respectivement le code source et des fichiers de données  
Premiers essais d'extraction d'un fichier ics d'ADE à partir d'une URL  
Attente d'un retour des enseignants pour obtenir un tableur d'export ADE  
Envoi d'un mail aux développeurs de l'application mobile Campus UGA afin d'obtenir quelques indications

## Séance 5 - Lundi 22 Février

Premier essai pour parser un fichier csv  
Recherches sur les librairies python permettant de traiter des formats excel (openpyxl, xlrd)  
Code d'export de fichier .xlsx en .csv  
Comparaison des données des exports d'ADE en iCalendar et excel (affichage tableau)

## Séance 6 - Mardi 23 Février

Analyse des descriptions de matières sur ADE et irrégularités dans les notations  
Prise en main de la bibliothèque Click (interfaces en ligne de commande)  
Apport de nouveaux éléments et reflexion sur la finalité du projet  
Premiers essais de parsage des descriptions de matières ADE  

## Séance 7 - Lundi 1er Mars

Documentation des fonctions  
Création d'un fichier contenant des expressions régulières  
Utilisation des regex pour identifier le type de chaque cours (TP/CM/TD...)  
Essais et lecture de la doc de click.choice()  
Extraction en csv des maquettes filières  

## Séance 8 - Mardi 2 Mars

Ajout de nouvelles fonctions permettant de choisir un groupe d'étudiants en affichant des listes de propositions  
Utilisation de click.choice() avec ces listes  
Préparation de la présentation de mi-parcours  

## Séance 9 - Lundi 8 Mars

Soutenance de mi-parcours  
Réorganisation des dossiers et fichiers de données  
Création d'une nouvelle fonctionnalité permettant d'extraire les codes APOGEE et les noms de matières depuis les maquettes filières  

## Séance 10 - Mardi 9 Mars

Ajout d'une nouvelle fonction permettant d'extraire les données d'un fichier csv vers un fichier JSON contenant un dictionnaire ayant pour clés les codes APOGEE et comme valeur des dictionnaires contenant le nom des matières et le nombre d'heure pour chaque type de cours.  
Modification de la fonction de conversion des fichiers xlsx en csv pour la rendre plus fonctionnelle en ajoutant par exemple une option permettant de vider les dossiers de données si celle-ci est activée.  
Amélioration du parsage de la description des matières ADE : utilisation d'une expression régulière pour réduire les erreurs possibles lors de la récupération des professeurs  

## Séance 11 - Lundi 15 Mars

Modification du parser des maquettes filières et ajout d'une fonction permettant de convertir les niveaux horaires en float dans le fichier baselines.json  
Appropriation de Poetry et premiers essais sur notre projet Python  
Ajout des premières dépendances à Poetry (click, requests, openpyxl, unidecode...)  

## Séance 12 - Mardi 16 Mars

Réflexion sur une meilleure structure de code et utilisation de Poetry  
Relecture de la documentation sur Poetry  
Discussion poussée avec le professeur, à faire pour la suite :  
Création d'un dictionnaire d'allias pour ajouter de la robustesse a l'application en cas d'erreur dans les noms de matières  
Ajout d'un fichier customization pour gérer différents cas spéciaux  

## Séance 13 - Lundi 22 Mars

Nouvelle fonction permettant la reconnaissance automatique des bonnes colonnes dans les maquettes filières  
Difficultés a utiliser Poetry : lecture supplémentaire de la doc  
Ajout de fonctionnalités pour détecter et informer des erreurs (informations manquantes dans ADE)  
Réflexion sur la classe main  

## Séance 14 - Mardi 23 Mars

Résolution des problèmes liés aux imports de modules et fichiers python  
Création d'un nouveau projet avec un nom ne comportant pas de tiret médian (cause de nos problèmes)  
Ajout des nouvelles dépendances à Poetry qui est désormais correctement initialisé  
Création d'un fichier .gitignore pour les fichier d'execution python et .lock de Poetry  
Travail sur click et la fonction main du projet  

## Séance 15 - Lundi 29 Mars

Refactoring du code  
Fonctions de calcul des volumes horaires par matière ou par enseignant  
Réflexion sur le moyen de sélectionner les informations utiles pour faire la comparaison avec les maquettes filières  
Travail sur l'interface avec click  
Rédaction du rapport
